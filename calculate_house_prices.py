# My script to calculate house prices in different areas

### NOTICE: install pandas with pip install pandas before running this script or this will fail.

import pandas ## I will convert csv reading to pandas in the next version

### ------------------
### ----- AREA 1 -----
### ------------------

# Open file
f = open("houses_area_1.csv")


# Parse house prices
i = 0
PRICES = []
for l in f:
    i += 1
    if i == 1:
        continue
    house_price = l.split(", ")[1]
    PRICES.append(float(house_price))

print(PRICES)
f.close()


# Calculate mean
mean1 = 0
houseCount = len(PRICES)
for i in range(houseCount):
    price = PRICES[i]
    mean1 += price
mean1 /= houseCount

print(mean1)



f = open("mean_price_area_1.txt", "w") #Open file
f.write(str( mean1)) #Write to file
f.close()

### ------------------
### ----- AREA 2 -----
### ------------------

# Open file
f = open("houses_area_2.csv")


# Parse house prices
i = 0
PRICES_2 = []
for l in f:
    i += 1
    if i == 1:
        continue
    house_price = l.split(", ")[1]
    PRICES_2.append(float(house_price))

print(PRICES_2)
f.close()


# Calculate mean
mean2 = 0
houseCount = len(PRICES_2)
for i in range(houseCount):
    price = PRICES_2[i]
    mean2 += price
mean2 /= houseCount

print(mean2)



f = open("mean_price_area_2.txt", "w") #Open file
f.write(str(mean2)) #Write to file
f.close()

### ------------------
### ----- TOTAL ------
### ------------------
# To calculate total get weighted average:
# m_total = (m1*n1 + m2*n2)/ (n1+n2)
m_total = (mean1*len(PRICES)+mean2*len(PRICES_2))/(len(PRICES) + len(PRICES_2))
print("total mean: " + str(m_total)+ " dollars")

f = open("mean_price_total.txt", "w") #Open file
f.write(str(m_total)) #Write to file
f.close()
